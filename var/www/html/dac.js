function DacCtrl($scope) {
  // The DAC table
  $scope.table = [
    [{text:'Bridge', bold:true, color:"#FBCF33"}, {text:'Flag Bridge', bold:true, color:"#FBCF33"}, {text:'Sensor', bold:true, color:"#B5E1BE"}, {text:'Damage Control', bold:true, color:"#B5E1BE"}, {text:'Aft Hull', bold:true, color:"#FDE8B8"}, {text:'Left Warp', bold:false, color:"#9DC9E4"}, {text:'Transporter', bold:false, color:"#FFFF10"}, {text:'Tractor', bold:false, color:"#FFFF10"}, {text:'Shuttle', bold:false, color:"#FFFF10"}, {text:'Lab', bold:false, color:"#FFFF10"}, {text:'Forward Hull', bold:false, color:"#FDE8B8"}, {text:'Right Warp', bold:false, color:"#9DC9E4"}, {text:'Excess Damage', bold:false, color:"#B5E1BE"}],

    [{text:'Drone', bold:true, color:"#F494C2"}, {text:'Phaser', bold:true, color:"#F494C2"}, {text:'Impulse', bold:false, color:"#9DC9E4"}, {text:'Left Warp', bold:false, color:"#9DC9E4"}, {text:'Right Warp', bold:false, color:"#9DC9E4"}, {text:'Aft Hull', bold:false, color:"#FDE8B8"}, {text:'Shuttle', bold:false, color:"#FFFF10"}, {text:'Damage Control', bold:true, color:"#B5E1BE"}, {text:'Center Warp', bold:false, color:"#9DC9E4"}, {text:'Lab', bold:false, color:"#FFFF10"}, {text:'Battery', bold:false, color:"#00A0EC"}, {text:'Phaser', bold:false, color:"#F494C2"}, {text:'Excess Damage', bold:false, color:"#B5E1BE"}],

    [{text:'Phaser', bold:true, color:"#F494C2"}, {text:'Transporter', bold:true, color:"#FFFF10"}, {text:'Right Warp', bold:false, color:"#9DC9E4"}, {text:'Impulse', bold:false, color:"#9DC9E4"}, {text:'Forward Hull', bold:false, color:"#FDE8B8"}, {text:'Aft Hull', bold:false, color:"#FDE8B8"}, {text:'Left Warp', bold:false, color:"#9DC9E4"}, {text:'APR', bold:false, color:"#9DC9E4"}, {text:'Lab', bold:false, color:"#FFFF10"}, {text:'Transporter', bold:false, color:"#FFFF10"}, {text:'Probe', bold:false, color:"#FFFF10"}, {text:'Center Warp', bold:false, color:"#9DC9E4"}, {text:'Excess Damage', bold:false, color:"#B5E1BE"}],

    [{text:'Right Warp', bold:true, color:"#9DC9E4"}, {text:'Aft Hull', bold:false, color:"#FDE8B8"}, {text:'Cargo', bold:false, color:"#FDE8B8"}, {text:'Battery', bold:false, color:"#00A0EC"}, {text:'Shuttle', bold:false, color:"#FFFF10"}, {text:'Torpedo', bold:true, color:"#F494C2"}, {text:'Left Warp', bold:false, color:"#9DC9E4"}, {text:'Impulse', bold:false, color:"#9DC9E4"}, {text:'Right Warp', bold:false, color:"#9DC9E4"}, {text:'Tractor', bold:false, color:"#FFFF10"}, {text:'Probe', bold:false, color:"#FFFF10"}, {text:'Any Weapon', bold:false, color:"#F494C2"}, {text:'Excess Damage', bold:false, color:"#B5E1BE"}],

    [{text:'Forward Hull', bold:false, color:"#FDE8B8"}, {text:'Impulse', bold:false, color:"#9DC9E4"}, {text:'Lab', bold:false, color:"#FFFF10"}, {text:'Left Warp', bold:false, color:"#9DC9E4"}, {text:'Sensor', bold:true, color:"#B5E1BE"}, {text:'Tractor', bold:false, color:"#FFFF10"}, {text:'Shuttle', bold:false, color:"#FFFF10"}, {text:'Right Warp', bold:false, color:"#9DC9E4"}, {text:'Phaser', bold:false, color:"#F494C2"}, {text:'Transporter', bold:false, color:"#FFFF10"}, {text:'Battery', bold:false, color:"#00A0EC"}, {text:'Any Weapon', bold:false, color:"#F494C2"}, {text:'Excess Damage', bold:false, color:"#B5E1BE"}],

    [{text:'Cargo', bold:false, color:"#FDE8B8"}, {text:'Forward Hull', bold:false, color:"#FDE8B8"}, {text:'Battery', bold:false, color:"#00A0EC"}, {text:'Center Warp', bold:false, color:"#9DC9E4"}, {text:'Shuttle', bold:false, color:"#FFFF10"}, {text:'APR', bold:false, color:"#9DC9E4"}, {text:'Lab', bold:false, color:"#FFFF10"}, {text:'Phaser', bold:false, color:"#F494C2"}, {text:'Any Warp', bold:false, color:"#9DC9E4"}, {text:'Probe', bold:false, color:"#FFFF10"}, {text:'Aft Hull', bold:false, color:"#FDE8B8"}, {text:'Any Weapon', bold:false, color:"#F494C2"}, {text:'Excess Damage', bold:false, color:"#B5E1BE"}],

    [{text:'Aft Hull', bold:false, color:"#FDE8B8"}, {text:'APR', bold:false, color:"#9DC9E4"}, {text:'Shuttle', bold:false, color:"#FFFF10"}, {text:'Right Warp', bold:false, color:"#9DC9E4"}, {text:'Scanner', bold:true, color:"#B5E1BE"}, {text:'Tractor', bold:false, color:"#FFFF10"}, {text:'Lab', bold:false, color:"#FFFF10"}, {text:'Left Warp', bold:false, color:"#9DC9E4"}, {text:'Phaser', bold:false, color:"#F494C2"}, {text:'Transporter', bold:false, color:"#FFFF10"}, {text:'Battery', bold:false, color:"#00A0EC"}, {text:'Any Weapon', bold:false, color:"#F494C2"}, {text:'Excess Damage', bold:false, color:"#B5E1BE"}],

    [{text:'Left Warp', bold:true, color:"#9DC9E4"}, {text:'Forward Hull', bold:false, color:"#FDE8B8"}, {text:'Cargo', bold:false, color:"#FDE8B8"}, {text:'Battery', bold:false, color:"#00A0EC"}, {text:'Lab', bold:false, color:"#FFFF10"}, {text:'Drone', bold:true, color:"#F494C2"}, {text:'Right Warp', bold:false, color:"#9DC9E4"}, {text:'Impulse', bold:false, color:"#9DC9E4"}, {text:'Left Warp', bold:false, color:"#9DC9E4"}, {text:'Tractor', bold:false, color:"#FFFF10"}, {text:'Probe', bold:false, color:"#FFFF10"}, {text:'Any Weapon', bold:false, color:"#F494C2"}, {text:'Excess Damage', bold:false, color:"#B5E1BE"}],

    [{text:'Phaser', bold:true, color:"#F494C2"}, {text:'Tractor', bold:true, color:"#FFFF10"}, {text:'Left Warp', bold:false, color:"#9DC9E4"}, {text:'Impulse', bold:false, color:"#9DC9E4"}, {text:'Aft Hull', bold:false, color:"#FDE8B8"}, {text:'Forward Hull', bold:false, color:"#FDE8B8"}, {text:'Right Warp', bold:false, color:"#9DC9E4"}, {text:'APR', bold:false, color:"#9DC9E4"}, {text:'Lab', bold:false, color:"#FFFF10"}, {text:'Transporter', bold:false, color:"#FFFF10"}, {text:'Probe', bold:false, color:"#FFFF10"}, {text:'Center Warp', bold:false, color:"#9DC9E4"}, {text:'Excess Damage', bold:false, color:"#B5E1BE"}],

    [{text:'Torpedo', bold:true, color:"#F494C2"}, {text:'Phaser', bold:true, color:"#F494C2"}, {text:'Impulse', bold:false, color:"#9DC9E4"}, {text:'Right Warp', bold:false, color:"#9DC9E4"}, {text:'Left Warp', bold:false, color:"#9DC9E4"}, {text:'Forward Hull', bold:false, color:"#FDE8B8"}, {text:'Tractor', bold:false, color:"#FFFF10"}, {text:'Damage Control', bold:true, color:"#B5E1BE"}, {text:'Center Warp', bold:false, color:"#9DC9E4"}, {text:'Lab', bold:false, color:"#FFFF10"}, {text:'Battery', bold:false, color:"#00A0EC"}, {text:'Phaser', bold:false, color:"#F494C2"}, {text:'Excess Damage', bold:false, color:"#B5E1BE"}],

    [{text:'Aux Control', bold:true, color:"#FBCF33"}, {text:'Emergency Bridge', bold:true, color:"#FBCF33"}, {text:'Scanner', bold:true, color:"#B5E1BE"}, {text:'Probe', bold:true, color:"#FFFF10"}, {text:'Forward Hull', bold:true, color:"#FDE8B8"}, {text:'Right Warp', bold:false, color:"#9DC9E4"}, {text:'Transporter', bold:false, color:"#FFFF10"}, {text:'Shuttle', bold:false, color:"#FFFF10"}, {text:'Tractor', bold:false, color:"#FFFF10"}, {text:'Lab', bold:false, color:"#FFFF10"}, {text:'Aft Hull', bold:false, color:"#FDE8B8"}, {text:'Left Warp', bold:false, color:"#9DC9E4"}, {text:'Excess Damage', bold:false, color:"#B5E1BE"}]
  ];

  // Indices used to skip bold entries.
  $scope.indices = [0,0,0,0,0,0,0,0,0,0,0];
  $scope.hits = [];
  $scope.splash = true;
  $scope.checkit = false;
  $scope.destroyed_systems = [];
/*
lab, shuttle, tractor, bridge            , color:"#FFFF10"
warp,                                    , color:"#9DC9E4"
sensor, damage control, excess damage    , color:"#B5E1BE"
weapons #F494C2                          , color:"#F494C2"
battery #00A0EC                          , color:"#00A0EC"
cargo, hull, white                       , color:"white"
*/

  $scope.isLast = function(row) {
    if($scope.indices[row] == 12) {
      return false;
    }
    return true;
  };

  $scope.markDestroyed = function(item, index) {
    var result = item.row;
    // Mark this system as destroyed
    $scope.destroyed_systems.push(item.text);

    $scope.indices[result]++;
    if($scope.indices[result] > 12) {
      $scope.indices[result] = 12;
    }
    // Loop through all of our hits
    for(var i=index; i<$scope.hits.length; i++) {
      // If the given row matches what is in our row, update the table.
    //  if($scope.hits[i].row === result) {
    //    $scope.hits[i] = {text:$scope.table[result][$scope.indices[result]].text, color:$scope.table[result][$scope.indices[result]].color, row:result, column:$scope.indices[result]};
    //  }
      // If the given text matches any other text in our table. recursively delete.
      if($scope.hits[i].text === item.text) {
        // Recursively call this function to destroy any other items which happen to share the same name.
        //$scope.markDestroyed($scope.hits[i], i);
        $scope.hits[i] = {text:$scope.table[result][$scope.indices[result]].text, color:$scope.table[result][$scope.indices[result]].color, row:result, column:$scope.indices[result], d1:$scope.hits[i].d1, d2:$scope.hits[i].d2};

      }

      if($scope.table[result][$scope.indices[result]].bold) {
        $scope.indices[result]++;
        if($scope.indices[result] > 12) {
          $scope.indices[result] = 12;
        }
      }


      // If any of our systems marked as destroyed currently appear in our hit list, recursively remove them. 
      for(var x=0; x<$scope.destroyed_systems.length; x++) {
        if($scope.destroyed_systems[x] === $scope.hits[i].text) {
          $scope.markDestroyed($scope.hits[i], i);
        }
      }
    }
    // We have changed some variables behind the scenes. Force a refresh of the page.
    $scope.$apply();
  };


  $scope.confirmDestroy = function(item, index) {
    bootbox.confirm("<h4>Captain?</h4><br />This action will mark all remaining <strong>" + item.text + "</strong> boxes destroyed for the remainder of this volley.<br /><br /><i>This action can not be undone.</i>", function(confirm) {
      if(confirm) {
        $scope.markDestroyed(item, index);
      }
      else {
        console.log("canceled");
      }
    }); 
  };

  $scope.findGlyph = function(value) {
    var result = "";
    switch(value){
      case 1:
        result = "\u2680";
        break;
      case 2:
        result = "\u2681";
        break;
      case 3:
        result = "\u2682";
        break;
      case 4:
        result = "\u2683";
        break;
      case 5:
        result = "\u2684";
        break;
      case 6:
        result = "\u2685";
        break;
      default:
        result = "X";
      }
      return result;
  }
  
  $scope.rollDice = function() {
    var d1 = Math.floor((Math.random()*6)+1);
    var d2 = Math.floor((Math.random()*6)+1);


    // Our dice results range from 2-12, but computers start counting at 0. So we'll simplify our lives and scale our die results to match the table indices. It will make everything easier going forward.
    return {value:(d1 + d2) - 2, die1:$scope.findGlyph(d1), die2:$scope.findGlyph(d2)};
  };

  $scope.volley = function() {
    // This function is called when the button is pressed. Clear out any previous results.
    $scope.splash = false;
    $scope.indices = [0,0,0,0,0,0,0,0,0,0,0];
    $scope.hits = [];

    // Loop through every hit, based on the user input. Roll the dice and apply the hit. Adjust the column indice for bold entries.
    for(var i=0; i<parseInt($scope.hitInput); i++) {
        // Get result of dice rolls. We chop it down from 2-12 to 0-10.
        var dice = $scope.rollDice();
        var result = dice.value;

        // Push the results into the hits array. We also store the row and column being used in case a given system is destroyed and the user needs to increment.
        $scope.hits.push({text:$scope.table[result][$scope.indices[result]].text, color:$scope.table[result][$scope.indices[result]].color, row:result, column:$scope.indices[result], d1:dice.die1, d2:dice.die2})

        // If the result of this particular hit was a BOLD entry, then we should increase the indice so as to not use it again this volley.
        if($scope.table[result][$scope.indices[result]].bold) {
            $scope.indices[result]++;
            if($scope.indices[result] > 12) {
              $scope.indices[result] = 12;
            }
        }
    }

    // Clean up. We need to reset some of the indices so that nothing is skipped later when the user tries to bypass a destroyed BOLD system.
    // This is because we 
    for(var i=0; i<$scope.indices.length; i++) {
      if($scope.indices[i] > 0) {
        $scope.indices[i]--;
      } 
    }
    
    // sort the list
    /*
    $scope.hits.sort(function (a, b) {
        if (a.text > b.text)
          return 1;
        if (a.text < b.text)
          return -1;
        // a must be equal to b
        return 0;
    });
    */
    $scope.hitInput = '';
  };
 
}
