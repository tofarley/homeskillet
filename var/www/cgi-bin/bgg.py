#!/usr/bin/env python3

import argparse
import time
import re
import requests
from lxml import objectify
from lxml.html import fromstring
import sys
import unicodedata
# Import modules for CGI handling 
import cgi, cgitb 

head = """
        <title>3D Printing for Boardgames</title>
        <!-- Latest compiled and minified CSS -->

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>

        <style>
            .loader {
                position: relative;
                left: 40%;
                top: 5%;
                z-index: 1;
                border: 16px solid #f3f3f3; /* Light grey */
                border-top: 16px solid #3498db; /* Blue */
                border-radius: 50%;
                width: 120px;
                height: 120px;
                animation: spin 2s linear infinite;
            }

            @keyframes spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
            }
        </style>
        <script>
            function myFunction() {
                var x = document.getElementById("myloader");
                if (x.style.display === "none") {
                    x.style.display = "block";
                } else {
                    x.style.display = "none";
                }
            }

            var options = {
  				valueNames: [ 'name', 'born' ]
			};

			var userList = new List('users', options);
			
        </script>
        """

def remove_accents(input_str):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    only_ascii = nfkd_form.encode('ASCII', 'ignore')
    return only_ascii

cgitb.enable(display=1, logdir="/var/log")
# Create instance of FieldStorage 
form = cgi.FieldStorage() 

# Get data from fields
username = form.getvalue('username')
print("Content-type:text/html\r\n\r\n")
print("<html>")
print("<head>")
print(head)
#print("<title>Hello - Second CGI Program</title>")
print("</head>")
print("<body>")
#print("<h2>Hello %s</h2>" % (username))
#parser = argparse.ArgumentParser()
#parser.add_argument("username", help="BGG username to check")
#args = parser.parse_args()
# Check status code instead. 202 means that the request was received. 200 means everything is good.

#username= args.username
# Need to check this output and ensure that the async processing has completed before continuing.
while True:
    r = requests.get('https://boardgamegeek.com/xmlapi/collection/' + username + '?own=1')
    #if "try again later" in r.text:
    if r.status_code == 202:
    	# Wait and try again later.
    	#print("Request sent to BGG for " + username + "'s collection. Please wait.")
    	time.sleep(5)
    else:
    	break
# Parse the xml. We need to remove the 'UTF-8' encoding specified in the xml or lxml will complain.
mygames = objectify.fromstring(r.text.replace('encoding="utf-8" ', ''))
#print("<h2>Hello %s</h2>" % (mygames.countchildren()))
# Show a count of games.
print('<div class="jumbotron">')
try:
	if len(mygames.item):
		print("<h1>" + username + " has " + str(mygames.countchildren()) + " games in their collection.</h1>")
except:
	print("<h1>" + username + " does not exist</h1>")
	sys.exit(1)
game_ids = []

for game in mygames.item:
	#print(game.attrib['objectid'], game.name)
	game_ids.append(game.attrib['objectid'])

while True:
    r = requests.get('http://boardgamegeek.com/xmlapi/geeklist/186909?comments=1')
    #if "try again later" in r.text:
    if r.status_code == 202:
    	# Wait and try again later.
    	#print("Request sent to BGG for 3d printing geeklist. Please wait.")
    	time.sleep(5)
    else:
    	break


printable_games = objectify.fromstring(r.text.replace('encoding="utf-8" ', ''))

#for game in printable_games.item:
	#print(game.attrib['objectid'], game.attrib['objectname'])


matching_games = [ game_list for game_list in printable_games.item if game_list.attrib['objectid'] in game_ids ]
print("<p>" + str(len(matching_games)) + " of those games have 3D-printable accessories listed on <a href='https://boardgamegeek.com/geeklist/186909'>3D Prints for Board Games</a>.</p>")
print("<p>")
print('<ul>')
matching_games.sort(key=lambda x: x.attrib['objectname'], reverse=False)
for game in matching_games:
	urls = []
	#print("<h2>" + game.attrib['objectname'] + "</h2>")
	#gamename = unicodedata.normalize('NFKD', game.attrib['objectname']).encode('ascii','ignore')
	#print(type(game.attrib['objectname']))
	#game_name = ''.join(c for c in unicodedata.normalize('NFD', game.attrib['objectname'])
	game_name = str(remove_accents(game.attrib['objectname']), 'utf-8')
	#print(game_name)
	
	print('<li><a href="https://boardgamegeek.com/geeklist/186909/item/' + game.attrib['id'] + '#item' + game.attrib['id'] + '">' + game_name + '</a><small> posted on ' + game.attrib['postdate'] + '</small></li>')
	print("<ul>")
	comments = game.getchildren()
	for comment in comments:

		#urls = urls + re.findall('(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))', str(comment))
		urls = urls + re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\), ]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', str(comment))
	

	for url in list(set(urls)):
		url = url.replace('[/url]', '').split(']')[0]
		#try:
		#	r = requests.get(url)
		#	tree = fromstring(r.content)
		#	#print(tree.findtext('.//title'))
		#	link_title = tree.findtext('.//title')
		#except OSError:
		#	link_title = "N/A"

		print('<li><a href="' + url + '">' + url + '</a></li>')
	print("</ul>")
print('</ul>')
print('</p>')
print('</div>')
print("</body>")
print("</html>")
