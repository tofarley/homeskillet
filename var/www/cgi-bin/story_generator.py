#!/usr/bin/env python3
from peewee import *
import unicodedata
import cgi, cgitb 

database = SqliteDatabase('/var/www/cgi-bin/story_generator.db', **{})

class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = database

class Prompts(BaseModel):
    author = CharField(null=True)
    line = CharField(null=True)
    title = CharField(null=True)

    class Meta:
        db_table = 'prompts'

class Settings(BaseModel):
    setting = CharField(null=True)

    class Meta:
        db_table = 'settings'

class Tics(BaseModel):
    tic = CharField(null=True)

    class Meta:
        db_table = 'tics'

class Goals(BaseModel):
    goal = CharField(null=True)

    class Meta:
        db_table = 'goals'

class Aspects(BaseModel):
    aspect = CharField(null=True)

    class Meta:
        db_table = 'aspects'

class Endings(BaseModel):
    ending = CharField(null=True)

    class Meta:
        db_table = 'endings'

class Characters(BaseModel):
    character = CharField(null=True)

    class Meta:
        db_table = 'characters'

class Events(BaseModel):
    event = CharField(null=True)

    class Meta:
        db_table = 'events'

class Places(BaseModel):
    place = CharField(null=True)

    class Meta:
        db_table = 'places'

class Things(BaseModel):
    thing = CharField(null=True)

    class Meta:
        db_table = 'things'

class SqliteSequence(BaseModel):
    name = UnknownField(null=True)  # 
    seq = UnknownField(null=True)  # 

    class Meta:
        db_table = 'sqlite_sequence'
        primary_key = False

class Topics(BaseModel):
    topic = CharField(null=True)

    class Meta:
        db_table = 'topics'

head = """
        <title>Story Generator</title>
        <!-- Latest compiled and minified CSS -->

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>

        <style>
            .loader {
                position: relative;
                left: 40%;
                top: 5%;
                z-index: 1;
                border: 16px solid #f3f3f3; /* Light grey */
                border-top: 16px solid #3498db; /* Blue */
                border-radius: 50%;
                width: 120px;
                height: 120px;
                animation: spin 2s linear infinite;
            }

            @keyframes spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
            }
            li{
                margin-top: 40px;
            }

            li:first-child {
                margin-top:0;
            }
        </style>
        <script>
            function myFunction() {
                var x = document.getElementById("myloader");
                if (x.style.display === "none") {
                    x.style.display = "block";
                } else {
                    x.style.display = "none";
                }
            }

            var options = {
                valueNames: [ 'name', 'born' ]
            };

            var userList = new List('users', options);
            
        </script>
        """

def remove_accents(input_str):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    only_ascii = nfkd_form.encode('ASCII', 'ignore')
    return str(only_ascii, 'utf-8')

cgitb.enable(display=1, logdir="/var/log")
# Create instance of FieldStorage 
form = cgi.FieldStorage() 

# Get data from fields
story_count = form.getvalue('stories')
print("Content-type:text/html\r\n\r\n")
print("<html>")
print("<head>")
print(head)
#print("<title>Hello - Second CGI Program</title>")
print("</head>")
print("<body>")
print('<div class="jumbotron">')
print("<p>")
print('<ol>')

for _ in range(int(story_count)):
    first = ""
    second = ""
    while first == second:
        first = Topics.select().order_by(fn.Random())[0].topic
        second = Topics.select().order_by(fn.Random())[0].topic
        setting = Settings.select().order_by(fn.Random())[0].setting
    #import pdb; pdb.set_trace()
    prompt = Prompts.select().order_by(fn.Random())[0]
    prompt_id = prompt.id
    line = prompt.line
    author = prompt.author
    title = prompt.title

    tic = Tics.select().order_by(fn.Random())[0].tic
    goal = Goals.select().order_by(fn.Random())
    character = Characters.select().order_by(fn.Random())[0].character
    aspect = Aspects.select().order_by(fn.Random())[0].aspect
    ending = Endings.select().order_by(fn.Random())[0].ending

    print("<li>It's like <span class='text-info'>{}</span> meets <span class='text-info'>{}</span> {}</li>".format(remove_accents(first), remove_accents(second), remove_accents(setting)))
    print("It's about a {} {} who wants to {} but must {}.".format(remove_accents(aspect), remove_accents(character), remove_accents(goal[0].goal), remove_accents(goal[1].goal)))
    print("<ul><li>Character trait: {}</li></ul>".format(remove_accents(tic)))
    print("<ul><small class='font-italic'><li>Opening line: '{}' - {}, {}</li></small></ul>".format(remove_accents(line), remove_accents(author), remove_accents(title)))
    print("Ending: {}".format(remove_accents(ending)))

    #print("-----")
print('</ol>')
print("</body>")
print("</html>")

