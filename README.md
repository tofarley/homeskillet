# homeskillet

A repository from homeskillet.org

## applications

This repository hosts three simple web applications that I wrote for my own personal use:

- sfb-dac: Damage Charts for a popular game based in our favorite sci-fi universe.
- bgg3dprints: Parse links from the [3D Prints for Board Games](https://boardgamegeek.com/geeklist/186909/3d-prints-board-games) GeekList at boardgamegeek.com
- story_generator: A fun little story generator framework for RPGs/short stories.

## deploying

The code can be deployed to any web server via ansible. It has been tested on Ubuntu 18.04 and Ubuntu 20.04.

Last installed via `ansible 2.9.13`:

```bash
IPADDR=<ip address of your web server>
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i ${IPADDR}, build.yml
```

> Note: The comma following the IP address in the ansible-playbook command is required for ansible to interpret the IP as a single-element list.

## usage

Sites are now available at:

- http://${IPADDR}/
- http://${IPADDR}/bgg3dprints
- http://${IPADDR}/story_generator
